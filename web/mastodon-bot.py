'''
Mastodon Bot
The bot will post the RSS feed of your choice into your mastodon timeline.
'''
import urllib.request
from mastodon import Mastodon
from bs4 import BeautifulSoup

# Create API instance
mastodon = Mastodon(client_id, client_secret, access_token,  api_base_url="https://mastodon.social")

#connect to the rss feed
def connect(url):
    headers = { 'User-Agent' : 'Mozilla/5.0' }

    with urllib.request.urlopen(urllib.request.Request(url, None, headers)) as url:
        page = url.read()
    return page

#keeping a feed.txt file as a database for seen feeds (to avoid dublicates in the timeline)
def seen(feed):
    with open("feed.txt", "r+") as file:
        if (feed not in [x.strip() for x in file.readlines()]):
            file.write(feed)
            file.write("\n")
            return False
        else:
            return True

#parse the titles and the urls of the feed
def parse_feed(url):
    data = connect(url)
    soup = BeautifulSoup(data, 'xml')
    items = soup.find_all('item')
    for item in items:
        #if is not in the feed.txt file
        if not seen(item.title.text):
            #toot it to mastodon
            mastodon.toot(item.title.text + "\n" + item.link.text)

#rss of choice hn frontpage from hnrss.org
rss_feed_url = "https://hnrss.org/frontpage"
parse_feed(rss_feed_url)

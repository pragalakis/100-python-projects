'''
Tumblr Image Scraper
Scrape all the images from a given tubmlr blog page (with a Tumblr api key).
'''
import urllib.request
import json
import workerpool

URL = "https://api.tumblr.com/v2/blog/[tumblr blog url]/posts/photo?api_key=[api key]&limit=50"

class DownloadJob(workerpool.Job):
    "Job for downloading a given URL."
    def __init__(self, url):
        self.url = url # The url we'll need to download when the job runs
    def run(self):
        image_name = self.url.split("/")[-1]
        save_to = "image_folder/" + image_name
        urllib.request.urlretrieve(self.url, save_to)
        print(image_name + " saved.")


# Initialize a pool, 5 threads in this case
pool = workerpool.WorkerPool(size=5)

def connect(url):
    headers = { 'User-Agent' : 'Mozilla/5.0' }

    with urllib.request.urlopen(urllib.request.Request(url, None, headers)) as url:
        page = url.read()
        data = json.loads(page.decode())

    return data

def scrape_images(offset):
    url_offset = URL + "&offset=" + str(offset)
    data = connect(url_offset)

    total_posts = data['response']['total_posts']
    # if posts still left
    if offset < total_posts:

        count = len(data["response"]["posts"])
        for i in range(count):
            image = data["response"]["posts"][i]["photos"][0]["original_size"]["url"]

            # Create a job to download the URL
            job = DownloadJob(image)
            pool.put(job)

        offset += 50
        # Call the function again
        scrape_images(offset)

    else:
        pass


offset = 0
scrape_images(offset)

# Send shutdown jobs to all threads, and wait until all the jobs have been completed
pool.shutdown()
pool.wait()

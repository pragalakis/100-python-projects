'''
Caesar cipher
Implement a Caesar cipher, both encoding and decoding. The key is an integer from 1 to 25. This cipher rotates the letters of the alphabet (A to Z). The encoding replaces each letter with the 1st to 25th next letter in the alphabet (wrapping Z to A). So key 2 encrypts "HI" to "JK", but key 20 encrypts "HI" to "BC". This simple "monoalphabetic substitution cipher" provides almost no security, because an attacker who has the encoded message can either use frequency analysis to guess the key, or just try all 25 keys.
'''
import string
from itertools import cycle
import functools

alphabet = string.ascii_lowercase



def ceasar(text, key):
    result = ""

    for l in text.lower():
        try:
            i = (alphabet.index(l) + key) % 26
            result += alphabet[i]
        except ValueError:
            result += l

    return result


def decrypt_ceasar(text, key):
    result = ""

    for l in text:
        try:
            i = (alphabet.index(l) - key) % 26
            result += alphabet[i]
        except ValueError:
            result += l

    return result


#command line

choice = input("1.Encrypt\n2.Decrypt\n")

if choice == "1":
    text = input("Text: ")
    key = input("Key: ")
    print(ceasar(text, int(key)))

elif choice == "2":
    cipher = input("Cipher: ")
    key = input("Keyword: ")
    print(decrypt_ceasar(cipher.lower(), int(key)))

else:
    print('Error input')
